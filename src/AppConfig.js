import Dashboard from './components/Dashboard/Dashboard';
import Orders from './components/Orders/Orders';
import OrderDetail from './components/OrderDetail/OrderDetail';
import Transactions from './components/Transactions/Transactions';
import Delivered from './components/Delivered/Delivered'

export const config = {
  views: [
    {
      label: 'Dashboard',
      viewId: 'dashboard',
      path: '/',
      component: Dashboard,
      isVisibleInSideNav: true,
      hiddenForRole: '',
      viewTitle: 'Overview Dashboard'
    },
    {
      label: 'Orders',
      viewId: 'orders',
      path: '/orders',
      component: Orders,
      isVisibleInSideNav: true,
      hiddenForRole: '',
      viewTitle: 'Orders'
    },
    {
      label: 'Order Details',
      viewId: 'orderDetails',
      path: '/orders/:orderId',
      component: OrderDetail,
      hiddenForRole: '',
      isVisibleInSideNav: false
    },
    {
      label: 'Settlements',
      viewId: 'transactionsList',
      path: '/settlements',
      component: Transactions,
      isVisibleInSideNav: true,
      hiddenForRole: 'Buyer',
      viewTitle: 'Settlements'
    },
    {
      label: 'Delivered',
      viewId: 'deliveredList',
      path: '/delivered',
      component: Delivered,
      isVisibleInSideNav: false,
      hiddenForRole: '',
      viewTitle: 'Delivered'
    },
  ],
  currentUser: {
    id: 1,
    role: "Buyer",
    name: "Jane Smith"
  }
};
