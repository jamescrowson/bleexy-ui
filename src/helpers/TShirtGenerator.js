import TShirtImage from '../assets/tshirt1.png';
import TShirtImage2 from '../assets/tshirt2.png';
import TShirtImage3 from '../assets/tshirt3.png';

export default class TShirtGenerator {
  generateWithId = (UPC) => {
    if (UPC === '671946184713') {
      return TShirtImage;
    } else if (UPC === '971546172654') {
      return TShirtImage2
    } else if (UPC === '887519512763') {
      return TShirtImage3
    }
  }
}
