import React from 'react';
import './TopNav.css';

import { AppContext } from '../../AppContext';

import BleexyLogo from '../../assets/bleexy_logo.png';
import BleexyLogoWhite from '../../assets/bleexy_logo_white.png';

import Login from '../../components/Login/Login';

const TopNav = () => (
  <AppContext.Consumer>
    {(context) => (
      <nav className={`navbar navbar-expand-lg mason-top-nav shadow-sm navbar-light ${context.state.currentUser.role === 'Buyer' ? 'bg-white' : 'bg-dark'}`}>
        <a className="navbar-brand" href="/"><img className="app-logo" alt="Bleexy logo" src={context.state.currentUser.role === 'Buyer' ? BleexyLogo : BleexyLogoWhite} /></a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
        </div>
        <Login/>
      </nav>
    )}
  </AppContext.Consumer>
);

export default TopNav;
