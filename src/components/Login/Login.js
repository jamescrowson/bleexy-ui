import React from 'react';
import './Login.css';

import { AppContext } from '../../AppContext';

import { NavLink } from 'react-router-dom';

const Login = () => (
  <div>
    <AppContext.Consumer>
      {(context) => (
        <div className="form-group mt-2">
          <div className="float-right">
              <form className="form-inline">
                <div className={`mr-3 ${context.state.currentUser.role === 'Buyer' ? '' : ' white'}`}>
                  Signed in as:
                </div>
                <div className="btn-group">
                  <button type="button" className="btn btn-light dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    { context.state.currentUser.name } ({ context.state.currentUser.role })
                  </button>
                  <div className="dropdown-menu dropdown-menu-right">
                    { context.state.users && context.state.users.map((user, i) => {
                      if (i === 0) {
                        return (
                          <React.Fragment key={i}>
                            <h6 className="dropdown-header">Buyers</h6>
                            <UserLink i={i} user={user} context={context} />
                          </React.Fragment>
                        )
                      }
                      if (context.state.users[i].role === 'Retailer' && context.state.users[i - 1].role === 'Buyer') {
                        return (
                          <React.Fragment key={i}>
                            <div className="dropdown-divider"></div>
                            <h6 className="dropdown-header">Retailers</h6>
                            <UserLink i={i} user={user} context={context} />
                          </React.Fragment>
                        )
                      } else {
                        return <UserLink key={i} i={i} user={user} context={context} />
                      }
                    }
                  )}
                  </div>
                </div>
              </form>
          </div>
        </div>
      )}
    </AppContext.Consumer>
  </div>
);

export default Login;

const UserLink = ({ i, user, context }) => (
  <NavLink
    key={i}
    onClick={() => context.setState('currentUser', user)}
    activeClassName='is-actives'
    className='dropdown-item'
    to='/'
  >
    {user.name} ({user.role})
  </NavLink>
)
