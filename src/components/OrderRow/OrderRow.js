import React, { Component } from 'react';
import './OrderRow.css';

import { currencyFormatter } from '../../helpers/Utilities';
import TShirtGenerator from '../../helpers/TShirtGenerator';

import APIService from '../../services/API.js';

class OrderRow extends Component {

  constructor(props) {
    super(props);
    this.state = {
      description: '',
      size: '',
      itemImage:''
    }
    this.apiService = new APIService();
    this.tshirtGenerator = new TShirtGenerator();
    this.getProductWithId = this.getProductWithId.bind(this);
  }

  componentDidMount() {
    this.getProductWithId(this.props.UPC);
  }

  async getProductWithId(UPC) {
    const response = await this.apiService.getProductWithId(UPC);
    this.setState({
      description: response.Description,
      size: response.ItemDetails[0].value,
      itemImageURL: response.ItemImageUri,
      quantity: this.props.quantity,
    });
  }

  render() {
    const { description, size } = this.state;
    const { quantity, price} = this.props;
    return (
      <tr>
        <td><img src={this.tshirtGenerator.generateWithId(this.props.UPC)} alt="tshirt"/></td>
        <td>{description}</td>
        <td>{size}</td>
        <td>{quantity}</td>
        <td>{currencyFormatter.format(price)}</td>
      </tr>
    );
  }

}

export default OrderRow;
