import React, { Component } from 'react';
import './Delivered.css';

import APIService from '../../services/API.js';

class Transactions extends Component {

  constructor(props) {
    super(props);
    this.state = {
      orders:[],
    }
    this.apiService = new APIService();
    this.getDelivered = this.getDelivered.bind(this);
    this.setOrderAsDelivered = this.setOrderAsDelivered.bind(this);
  }

  componentDidMount() {
    this.getDelivered();
  }

  async getDelivered() {
    const ordersResponse = await this.apiService.getDelivered();
    this.setState({ orders: ordersResponse });
  }

  async setOrderAsDelivered(orderRowIndex) {
    const order = this.state.orders[orderRowIndex];
    const delivered = {
      OrderId: order.OrderID,
      TransactionDate: order.DeliveryDate,
    };
    await this.apiService.setDelivered(delivered);
    this.setState({ orders: [] });
    this.getDelivered();
  }

  renderView() {
    const { orders } = this.state;
    return (
      <div>
        <div className="card">
          <div className="row mt-4">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Order ID</th>
                  <th scope="col">Buyer ID</th>
                  <th scope="col">Retailer ID</th>
                  <th scope="col">Fulfillment ID</th>
                  <th scope="col">Status</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
                <tbody>
                  { orders.map((order, i) => <OrderRow key={i} index={i} orderId={order.OrderID} buyerId={order.BuyerID} retailerId={order.RetailerID} fullfillmentID={order.FulfilledByRetailerID} status={order.OrderStatus} handleClick={this.setOrderAsDelivered}/>)}
                </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-4">
          <div className="col-12">
            <h2>{this.props.title}</h2>
          </div>
        </div>
        <div className="row mt-4">
          <div className="col-12">
            <div className="table-responsive">
              {this.renderView()}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const OrderRow = ({index, orderId, buyerId, retailerId, fullfillmentID, status, handleClick}) => (
  <tr>
    <td>{orderId}</td>
    <td>{buyerId}</td>
    <td>{retailerId}</td>
    <td>{fullfillmentID}</td>
    <td>{status}</td>
    <td>{status <= 3 && <button type="button" className="btn btn-success" onClick={()=> handleClick(index)}>Mark as Delivered</button>}</td>
  </tr>
)


export default Transactions;
