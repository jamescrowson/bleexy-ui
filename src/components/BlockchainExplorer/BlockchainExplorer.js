import React, { Component } from 'react';
import './BlockchainExplorer.css';

import $ from 'jquery';
import Sockette from 'sockette';
import { WS_ENDPOINT } from '../../services/Constants.js';

import BlockchainBlock from './BlockchainBlock.js';

class BlockchainExplorer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      blocks: [],
    };
    this.handleNewBlockCreated = this.handleNewBlockCreated.bind(this);
  }

  componentDidMount() {
    $(function () {
      $('[data-toggle="popover"]').popover()
    });
    this.setupWebSocket();
  }

  setupWebSocket() {
    new Sockette(WS_ENDPOINT, {
      timeout: 5e3,
      maxAttempts: 10,
      onmessage: e => this.handleNewBlockCreated(e),
    });
  }

  handleNewBlockCreated(block) {
    const parsedBlock = JSON.parse(block.data);
    let newBlock = {
      id: parsedBlock.block_id,
      transactionId: parsedBlock.txs[0].tx_id,
      timestamp: parsedBlock.txs[0].timestamp,
      creatorId: parsedBlock.txs[0].creator_msp_id,
    };
    let blocks = this.state.blocks;
    blocks.push(newBlock);
    this.setState({ blocks });
    $(function () {
      $('[data-toggle="popover"]').popover()
    });
  }

  render() {
    let { blocks } = this.state;
    return (
      <div className={"blockchain-explorer " + (blocks.length > 0 ? 'blockchain-explorer-visible' : '')}>
        <div className="container-fluid">
          <div className="row">
            <ul className="list-inline blockchain-list">
              { blocks.length > 0 && blocks.map((block, i) => <BlockchainBlock  key={i} {...block} />) }
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default BlockchainExplorer;
