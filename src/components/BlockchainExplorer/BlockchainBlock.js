import React, { Component } from 'react';
import './BlockchainBlock.css';

class BlockchainBlock extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isCreated: false,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ isCreated: true })
    }, 2000);
  }

  render() {
    return <li
      className={"blockchain-block " + (this.state.isCreated ? 'blockchain-block-added' : '')}
      data-toggle="popover"
      data-placement="top"
      title={`Block ID: ${this.props.id}`}
      data-trigger="hover"
      data-html="true"
      data-content={`<li class="block">TransactionID: ${this.props.transactionId}</li><li class="block">Timestamp: ${this.props.timestamp}</li><li class="block">Creator: ${this.props.creatorId}</li>`}>
      {this.props.id}
    </li>;
  }
}

export default BlockchainBlock;
