import React, { Component } from 'react';
import './Dashboard.css';

import { Line } from 'react-chartjs-2';

import APIService from '../../services/API.js';
import {
  totalSalesLineChartConfig,
  DataAnalytics
} from '../../services/DataAnalytics.js';
import Spinner from '../../components/Spinner/Spinner';

class Dashboard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      orders: [],
      totalSalesChartData: {},
    }
    this.apiService = new APIService();
    this.dataAnalytics = new DataAnalytics();
    this.formatChartData = this.formatChartData.bind(this);
    this.setupCharts = this.setupCharts.bind(this);
  }

  componentDidMount() {
    this.getOrders();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.currentUser !== this.props.currentUser) {
      this.getOrders();
    }
  }

  async getOrders() {
    this.setState({ isLoading: true });
    const ordersResponse = this.props.currentUser.role === 'Buyer' ? await this.apiService.getOrdersForBuyerWithId(this.props.currentUser.id) : await this.apiService.getOrdersForRetailerWithId(this.props.currentUser.id)
    this.setState({
      isLoading: false,
      orders: ordersResponse,
    });
    this.setupCharts();
  }

  setupCharts() {
    this.setState({
      totalSalesChartData: totalSalesLineChartConfig,
    });
    this.formatChartData();
  }

  formatChartData() {
    this.setState({
      totalSalesChartData: {
        datasets: [
          {
            data: this.dataAnalytics.formatOrdersTotalData(this.state.orders),
          }
        ]
      }
    });
  }

  renderView() {
    return (
      <div className="mt-4 mb-3">
        <div className="row">
          <div className="col-12 col-lg-4">
            <div className="card">
              <h3 className="pb-2">{this.props.currentUser.role === 'Buyer' ? 'Total Spending' : 'Total Revenue'}</h3>
              <Line options={{ legend: { display: false} }} data={this.state.totalSalesChartData} />
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-4">
          <div className="col-12">
            <h2>{this.props.title}</h2>
          </div>
        </div>
        { this.state.isLoading ? <Spinner /> : this.renderView() }
      </div>
    );
  }

}

export default Dashboard;
