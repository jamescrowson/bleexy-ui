import React, { Component } from 'react';
import './Transactions.css';

import moment from 'moment'
import { currencyFormatter } from '../../helpers/Utilities';

import Spinner from '../../components/Spinner/Spinner';

import APIService from '../../services/API.js';

class Transactions extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      transactions: {},
    }
    this.apiService = new APIService();
    this.getTransactions = this.getTransactions.bind(this);
  }

  componentDidMount() {
    this.getTransactions();
  }

  async getTransactions() {
    const transactionsResponse = await this.apiService.getTransactions(this.props.currentUser.id);
    let settlements = [];
    for (var transaction in transactionsResponse) {
      let settlement = {
        orderId: transaction,
        values: transactionsResponse[transaction]
      }
      settlements.push(settlement);
    }
    this.setState({
      transactions: transactionsResponse,
      settlements,
      isLoading: false,
    });
  }

  renderView() {
    const {settlements} = this.state;
    return settlements.map((settlement, i) => <SettlementTable key={i} orderId={settlement.orderId} settlements={settlement.values} />)
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-4">
          <div className="col-12">
            <h2>{this.props.title}</h2>
          </div>
        </div>
        <div className="row mt-4">
          <div className="col-12">
            <div className="table-responsive">
              { this.state.isLoading ? <Spinner /> : this.renderView() }
            </div>
          </div>
        </div>
      </div>
    )
  }

}

const SettlementTable = ({ orderId, settlements }) => (
  <div className="my-4">
    <div className="card shadow-sm">
      <h2>Order #: {orderId}</h2>
      <table className="table mt-2">
        <thead>
          <tr>
            <th scope="col">Settlement ID</th>
            <th scope="col">Date</th>
            <th scope="col">From ID</th>
            <th scope="col">To ID</th>
            <th scope="col">Order ID</th>
            <th scope="col">Credit Amount</th>
            <th scope="col">Debit Amount</th>
          </tr>
        </thead>
        <tbody>
          {
            settlements.map((transaction, i) =>
            <TransactionRow
              key={i}
              transactionId={transaction.SettlementID}
              transactionDate={transaction.TransactionDate}
              fromId={transaction.FromID}
              toId={transaction.ToID}
              orderId={transaction.OrderID}
              creditAmount={transaction.CreditAmount.Amount}
              debitAmount={transaction.DebitAmount.Amount}
            />)
          }
        </tbody>
      </table>
    </div>
  </div>
)

const TransactionRow = ({transactionId, transactionDate, fromId, toId, orderId, creditAmount, debitAmount}) => (
  <tr>
    <td>{transactionId}</td>
    <td>{moment(transactionDate).format('MMMM Do YYYY')}</td>
    <td>{fromId}</td>
    <td>{toId}</td>
    <td>{orderId}</td>
    <td>{currencyFormatter.format(creditAmount)}</td>
    <td>{currencyFormatter.format(debitAmount)}</td>
  </tr>
)


export default Transactions;
