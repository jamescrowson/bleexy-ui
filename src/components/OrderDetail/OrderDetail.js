import React, { Component } from 'react';
import './OrderDetail.css';

import { AppContext } from '../../AppContext';

import moment from 'moment'
import { currencyFormatter } from '../../helpers/Utilities';

import OrderRow from '../../components/OrderRow/OrderRow';
import Spinner from '../../components/Spinner/Spinner';

import APIService from '../../services/API.js';

class OrderDetail extends Component {

  constructor(props) {
    super(props);
    this.state = {
      order: '',
    };
    this.apiService = new APIService();
    this.getOrderDetail = this.getOrderDetail.bind(this);
    this.renderView = this.renderView.bind(this);
  }

  componentDidMount() {
    this.getOrderDetail();
  }

  async getOrderDetail() {
    const orderDetail = await this.apiService.getOrderWithId(this.props.match.params.orderId, this.props.currentUser.id);
    const buyerDetail = await this.apiService.getBuyerDetailsWithId(orderDetail.BuyerID);
    const retailerDetail = await this.apiService.getRetailerWithId(orderDetail.RetailerID);
    this.setState({
      order: orderDetail,
      buyer: buyerDetail,
      retailer: retailerDetail
    });
  }

  renderView(context) {
    const { CarrierName, TrackingNumber } = this.state.order.ShippingMethod
    const billingAddress = {
      name: this.state.buyer.BuyerName,
      telephone: this.state.buyer.BuyerPhone.PhoneNumber,
      street: this.state.buyer.BuyerBillingAddress.StreetAddress,
      city: this.state.buyer.BuyerBillingAddress.City,
      state: this.state.buyer.BuyerBillingAddress.State,
      postalCode: this.state.buyer.BuyerBillingAddress.PostalCode,
      email: this.state.buyer.BuyerEmail,
    }
    return (
      <div>
        <div className="card mb-4">
          <div className="row">
            <div className="col">
              <div className="mt-2 float-left">
                <OrderStatus orderStatus={this.state.order.OrderStatus}/>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <OrderSummary
                orderId={this.state.order.OrderID}
                orderDate={moment(this.state.order.OrderDate).format('MMMM Do YYYY')}
                orderedFromRetailer={this.state.order.RetailerID}
                fulfilledByRetailer={this.state.order.FulfilledByRetailerID}
                shippingMethod={this.state.order.ShippingMethod.ServiceType}
                deliveryDate={moment(this.state.order.DeliveryDate).format('MMMM Do YYYY')}
                context={context}
              />
            { context.state.currentUser.role !== 'Buyer' &&
                <ShippingAddress
                  name={this.state.buyer.BuyerName}
                  streetAddress={this.state.buyer.BuyerShippingAddress.StreetAddress}
                  city={this.state.buyer.BuyerShippingAddress.City}
                  state={this.state.buyer.BuyerShippingAddress.State}
                  postalCode={this.state.buyer.BuyerShippingAddress.PostalCode}
                  phoneNumber={this.state.buyer.BuyerPhone.PhoneNumber}
                  email={this.state.buyer.BuyerEmail}
                />
              }
            </div>
          </div>
          <br />

          { context.state.currentUser.role === 'Buyer' && 'You can return or exchange an item from the order within 30 business days of receipt' }

          <h4 className="mt-4 mb-4">Order Summary</h4>
          <div className="table">
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Item</th>
                  <th scope="col">Description</th>
                  <th scope="col">Size</th>
                  <th scope="col">Qty</th>
                  <th scope="col">Price</th>
                </tr>
              </thead>
              <tbody>
                { this.state.order.OrderItems.map((order, i) =>
                  <OrderRow
                    key={i}
                    UPC={order.UPC}
                    quantity={order.OrderQuantity}
                    price={order.OrderPrice.Amount}
                  />)
                }
              </tbody>
            </table>
            <div className="row mt-4">
              <div className="col-md-9">
              </div>
              <div className="col-md-3 float-left">
                 <span className="total">Total: </span>{currencyFormatter.format(this.state.order.TotalAmountPaid.Amount)}
              </div>
            </div>
            <div className="row">
              <div className="mt-4 col-md-9">
                { (context.state.currentUser.id === this.state.order.RetailerID || context.state.currentUser.role === 'Buyer') &&
                  <PaymentSummary
                    creditCardType={this.state.order.OrderPaymentSummary.Payment.Type}
                    creditCardNumber={this.state.order.OrderPaymentSummary.Payment.Number}
                    name={billingAddress.name}
                    street={billingAddress.street}
                    city={billingAddress.city}
                    state={billingAddress.state}
                    zip={billingAddress.postalCode}
                    tel={billingAddress.telephone}
                    email={billingAddress.email}
                  />
                }
              </div>
            </div>
            <div className="row">
              <div className="mt-4 col-md-12">
                <ShippingSummary carrier={CarrierName} trackingNumber={TrackingNumber}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  render() {
    return (
      <AppContext.Consumer>
        {(context) => (
          <div className="container-fluid">
            <div className="row mt-4">
              <div className="col-12">
                <h2>Order Detail</h2>
              </div>
            </div>
            <div className="row mt-4">
              <div className="col-12">
                { this.state.order === "" ? <Spinner /> : this.renderView(context) }
              </div>
            </div>
          </div>
        )}
      </AppContext.Consumer>
    )
  }

}

export default OrderDetail;

const OrderSummary = ({ orderId, orderDate, orderedFromRetailer, fulfilledByRetailer, shippingMethod, deliveryDate, context }) => (
  <div className="mt-4 float-left">
    <strong>Order #:</strong> {orderId}
    <br/>
    <strong>Order date:</strong> {orderDate}
    <br/>
    <strong>Ordered from:</strong> Retailer {orderedFromRetailer}
    <br/>
    { context.state.currentUser.role !== 'Buyer' && <div><strong>Fulfilled by:</strong> Retailer {fulfilledByRetailer}<br/></div> }
    <br />
    { context.state.currentUser.role !== 'Buyer' && <div><strong>Method:</strong> {shippingMethod} <br/></div> }
    <div><strong>Delivery Date:</strong> {deliveryDate} <br/></div>
  </div>
);

const ShippingAddress = ({ name, streetAddress, city, state, postalCode, phoneNumber, email }) => (
  <div className="mt-4 pr-5 float-right">
    <strong>Shipping address:</strong>
    <br/>
    {name}
    <br/>
    {streetAddress}
    <br/>
    {city}, {state} {postalCode}
    <br/>
    Phone: {phoneNumber}
    <br/>
    Email: {email}
    <br/>
  </div>
);

const PaymentSummary = ({ name, street, city, state, zip, tel, creditCardType, creditCardNumber}) => (
  <div className="float-left">
    <h5>Payment Summary</h5>
    <p>{creditCardType} {creditCardNumber}</p>
    <p>
      {name}<br/>
      {street}<br/>
      {city}, {state} {zip}<br/>
      {tel}
    </p>
  </div>
);

const ShippingSummary = ({ carrier, trackingNumber }) => (
  <div className="float-left">
    <span className="pr-2">Carrier: { carrier }</span><span>Tracking Number: <a href="https://wwwapps.ups.com/WebTracking/track" target="_blank" rel="noopener noreferrer">{ trackingNumber }</a></span>
  </div>
)

const OrderStatus = ({ orderStatus }) => {
  let orderStatusString = '';
  switch (orderStatus) {
    case 1:
      orderStatusString = 'Ordered';
      break;
    case 2:
      orderStatusString = 'Fulfilled';
      break;
    case 3:
      orderStatusString = 'Shipped';
      break;
    case 4:
      orderStatusString = 'Delivered';
      break;
    default:
      orderStatusString = 'Ordered';
  }
  return <h3 className="order-status"><span className="badge badge-success">{ orderStatusString }</span></h3>
}
