import React, { Component } from 'react';
import './Orders.css';

import moment from 'moment'
import { currencyFormatter } from '../../helpers/Utilities';
import TShirtGenerator from '../../helpers/TShirtGenerator';

import Spinner from '../../components/Spinner/Spinner';

import APIService from '../../services/API.js';

class Orders extends Component {

  constructor(props) {
    super(props);
    this.state = {
      orders: [],
    };
    this.apiService = new APIService();
    this.tshirtGenerator = new TShirtGenerator();
    this.grabOrders = this.grabOrders.bind(this);
    this.goToOrderWithId = this.goToOrderWithId.bind(this);
  }

  componentDidMount() {
    this.grabOrders();
  }

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.currentUser.id !== this.props.currentUser.id) {
			this.grabOrders();
		}
	}

  async grabOrders() {
		this.setState({ orders: [] });
		let ordersResponse = [];
		if (this.props.currentUser.role === 'Buyer') {
			ordersResponse = await this.apiService.getOrdersForBuyerWithId(this.props.currentUser.id);
      // ordersResponse = await this.apiService.getOrders();
		} else if (this.props.currentUser.role === 'Retailer') {
			ordersResponse = await this.apiService.getOrdersForRetailerWithId(this.props.currentUser.id);
		}
		this.setState({
			orders: ordersResponse,
		});
  }

  goToOrderWithId(orderId) {
    this.props.history.push(`/orders/${orderId}`)
  }

  renderView() {
    return (
      <div>
        <div className="card">
          <div className="row">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col"></th>
                  <th scope="col">Order</th>
                  <th scope="col">Date</th>
                  <th scope="col">Shipping</th>
                  <th scope="col">Total</th>
                </tr>
              </thead>
                <tbody>
            			{this.state.orders && this.state.orders.map(order =>
                    <OrderRow key={order.OrderID}
                      match={this.props.match}
                      handleClick={this.goToOrderWithId}
                      tshirt={this.tshirtGenerator.generateWithId(order.OrderItems[0].UPC)}
                      {...order}
                    />)}
                </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-4">
          <div className="col-12">
            <h2>{this.props.title} ({this.state.orders.length})</h2>
          </div>
        </div>
        <div className="row mt-4 mb-4">
          <div className="col-12">
            <div className="table-responsive">
            	{ this.state.orders.length === 0 ? <Spinner /> : this.renderView() }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const OrderRow = ({ OrderID, OrderDate, ShippingMethod, TotalAmountPaid, tshirt, handleClick }) => (
  <tr onClick={() => handleClick(OrderID)}>
    <td>
      <img src={tshirt} alt="tshirt" />
    </td>
    <th scope="row">{OrderID}</th>
    <td>{moment(OrderDate).format('MMMM Do YYYY')}</td>
    <td><span className="badge badge-primary">{ShippingMethod.ServiceType}</span></td>
    <td>{currencyFormatter.format(TotalAmountPaid.Amount)}</td>
  </tr>
)


export default Orders;
