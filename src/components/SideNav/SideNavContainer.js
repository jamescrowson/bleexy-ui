import React from 'react';
import './SideNavContainer.css';

import { NavLink } from 'react-router-dom';

import { AppContext } from '../../AppContext';

const SideNavContainer = () => (
  <div className="mason-side-nav">
    <nav className="nav flex-column">
      <AppContext.Consumer>
        {(context) => (
          context.state.views.map(view =>  view.isVisibleInSideNav && view.hiddenForRole !== context.state.currentUser.role ? <NavItem key={view.viewId} label={view.label} path={view.path} /> : <React.Fragment key={view.viewId}/>)
        )}
      </AppContext.Consumer>
    </nav>
  </div>
);

export default SideNavContainer;

const NavItem = ({ label, path }) => (
  <NavLink exact activeClassName='is-active' to={`${path}`}>
    { label }
  </NavLink>
);
