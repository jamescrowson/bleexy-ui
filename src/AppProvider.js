import React, { Component } from 'react';
import { AppContext } from './AppContext';
import { config } from './AppConfig';

class AppProvider extends Component {

  constructor(props) {
    super(props);
    this.state = config;
  }
  
  render() {
    return (
      <AppContext.Provider value={{
        state: this.state,
        setState: (statePropToUpdate, value) => this.setState({ [statePropToUpdate]: value }),
      }}>
        { this.props.children }
      </AppContext.Provider>
    )
  }
}

export default AppProvider;
