import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import AppProvider from './AppProvider';
import { AppContext } from './AppContext';

import './App.css';

import APIService from './services/API.js';

import SideNavContainer from './components/SideNav/SideNavContainer';
import TopNav from './components/TopNav/TopNav';
import Spinner from './components/Spinner/Spinner';
import BlockchainExplorer from './components/BlockchainExplorer/BlockchainExplorer';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
    this.apiService = new APIService();
    this.getUsers = this.getUsers.bind(this);
  }

  componentDidMount() {
    this.getUsers();
  }

  async getUsers() {
    const userResponse = await this.apiService.getUsers();
    const appProvider = new AppProvider();
    appProvider.state.users = userResponse;
    this.setState({
      isLoading: false,
    });
  }

  renderView() {
    return (
      <AppProvider>
        <Router>
          <div className="App">
            <TopNav />
            <div className="container-fluid">
              <div className="row">
                <div className="col-2 d-none d-md-block no-gutter side-nav">
                  <SideNavContainer />
                </div>
                <main className="col-sm-12 col-md-10">
                  <AppContext.Consumer>
                    {(context) => (
                      context.state.views.map(view =>
                        <Route
                          key={view.viewId}
                          exact
                          path={view.path}
                          render={props => {
                            const View = view.component;
                            return <View title={view.viewTitle} currentUser={context.state.currentUser}  {...props} />
                          }}
                        />)
                    )}
                  </AppContext.Consumer>
                </main>
              </div>
            </div>
            <BlockchainExplorer />
          </div>
        </Router>
      </AppProvider>
    )
  }

  render() {
    return this.state.isLoading ? <Spinner /> : this.renderView();
  }

}

export default App;
