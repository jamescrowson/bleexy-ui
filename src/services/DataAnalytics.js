import moment from 'moment'

export const totalSalesLineChartConfig = {
  labels: ['Apr', 'May', 'Jun', 'Jul', 'Aug'],
  datasets: [
    {
      fill: false,
      borderColor: '#541388',
      borderWidth: 3,
      hoverBorderColor: 'rgba(255,99,132,1)'
    }
  ]
};


export class DataAnalytics {

  formatOrdersTotalData = (orders) => {
    let amounts = { Apr: 0, May: 0, Jun: 0, Jul: 0, Aug: 0 };
    orders.map(order => {
      const month = moment(order.OrderDate).format('MMM');
      return amounts[month] += order.TotalAmountPaid.Amount;
    });
    return [amounts.Apr, amounts.May, amounts.Jun, amounts.Jul, amounts.Aug];
  }

}
