import { API_ENDPOINT,  HEADERS } from './Constants.js';

export default class API {

  getOrders = async () => await (await fetch(`${API_ENDPOINT}/orderlist`)).json();

  getOrdersForBuyerWithId = async (buyerId) => await (await fetch(`${API_ENDPOINT}/buyer/${buyerId}/orderlist`)).json();

  getOrdersForRetailerWithId = async (retailerId) => await (await fetch(`${API_ENDPOINT}/retailer/${retailerId}/orderlist`)).json();

  getTransactions = async (retailerId) => await (await fetch(`${API_ENDPOINT}/settlements/retailer/${retailerId}`)).json();

  getDelivered = async () => await (await fetch(`${API_ENDPOINT}/orderlist`)).json();

  setDelivered = async (delivered) => await (
  await fetch(`${API_ENDPOINT}/delivered`, {
    headers: HEADERS(),
    method: 'POST',
    body: JSON.stringify(delivered)
  })).json();

  getOrderWithId = async (orderId, storeId) => await (await fetch(`${API_ENDPOINT}/order/${orderId}/${storeId}`)).json();

  getProductWithId = async (productId) => await (await fetch(`${API_ENDPOINT}/productmaster/${productId}`)).json();

  getRetailerWithId = async (retailerId) => await (await fetch(`${API_ENDPOINT}/retailer/${retailerId}`)).json();

  getBuyerDetailsWithId = async (buyerId) => await (await fetch(`${API_ENDPOINT}/buyer/${buyerId}`)).json();

  getUsers = async () => await (await fetch(`${API_ENDPOINT}/userlist`)).json();

}
