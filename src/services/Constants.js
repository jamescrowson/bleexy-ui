export const API_ENDPOINT = 'http://172.126.120.180:6051';
export const WS_ENDPOINT = 'ws://172.126.120.180:6051';
export const HEADERS = () => new Headers({ 'content-type': 'application/json', 'cache-control': 'no-cache' });
